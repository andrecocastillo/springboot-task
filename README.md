# EJERCICIO DE SPRINGBOOT - CRUD DE TASK
El presente proyecto presenta un crud de tareas usando Springboot, pruebas unitarias y sawagger.

## Instalacion
Haz clone del proyecto https://gitlab.com/andrecocastillo/springboot-task. luego abrelo con tu ID favorita y corre el servidor. La URL del proyecto es http://localhost:8080/ y para ver el api de servicios puede consultar la siguiente ruta http://localhost:8080/swagger-ui/index.html.

El nombre de la base de datos del proyecto es **taskexercise**, Si lo quieres cambiar puedes ir al archivo application.properties.

El siguiente codigo crea la tabla de las tareas

```mysql
DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_created` datetime NOT NULL,
  `state` enum('ACTIVE','INACTIVE') NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1
```
## Screenshots
![image](https://drive.google.com/uc?export=view&id=1vY0mtVREVudNZwq9DkqbHfz_hQ_fTT8z)
![image](https://drive.google.com/uc?export=view&id=1NBRsxLmVN4ZWILIWUM0PGhfK91-sKdJf)
![image](https://drive.google.com/uc?export=view&id=1Ay9xsovGU7UXL86mubgRKAS2nBBsWPx1)

## Autor
Andres Castillo