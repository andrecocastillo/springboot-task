-- Adminer 4.7.6 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `task`;
CREATE TABLE `task` (
  `task_id` int(11) NOT NULL AUTO_INCREMENT,
  `description` text NOT NULL,
  `date_created` datetime NOT NULL,
  `state` enum('ACTIVE','INACTIVE') NOT NULL,
  PRIMARY KEY (`task_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `task` (`task_id`, `description`, `date_created`, `state`) VALUES
(13,	'Realizacion del insert del codigo de Crud',	'2022-01-18 20:15:42',	'ACTIVE');

-- 2022-01-19 01:15:57
