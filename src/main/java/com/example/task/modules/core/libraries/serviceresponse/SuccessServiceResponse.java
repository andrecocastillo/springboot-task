package com.example.task.modules.core.libraries.serviceresponse;


public class SuccessServiceResponse extends ServiceResponse{

    public SuccessServiceResponse(Object data_response, String message){
        this.response = true;
        this.status_code = 200;
        this.message = message;
        this.setDataResponse(data_response);
    }

}
