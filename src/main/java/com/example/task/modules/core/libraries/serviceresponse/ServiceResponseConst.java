package com.example.task.modules.core.libraries.serviceresponse;

public class ServiceResponseConst {
    public static final String SUCCESS = "SUCCESS";
    public static final String ERROR = "ERROR";
    public static final String BAD_REQUEST = "BAD_REQUEST";
}
