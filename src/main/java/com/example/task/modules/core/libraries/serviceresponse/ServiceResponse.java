package com.example.task.modules.core.libraries.serviceresponse;

import com.google.gson.Gson;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public abstract class ServiceResponse{

    protected Boolean response;
    protected Integer status_code;
    protected String message;
    protected Object data_response;

    public ResponseEntity<String> responseJSON(){
		HttpHeaders responseHeaders = new HttpHeaders();
	    responseHeaders.set("Content-Type","application/json");
		String result_string = this.toString();
        if(this.response){
            return ResponseEntity.ok().headers(responseHeaders).body(result_string);
        }else{
            return ResponseEntity.status(HttpStatus.CONFLICT).headers(responseHeaders).body(result_string);
        }
    }

    public void setDataResponse(Object data_response){
        this.data_response = new Gson().toJson(data_response);
    }

	public String toString() {
		if(this.data_response == null){ this.data_response = "[]"; }
        return "{\"res\":\""+this.response+"\",\"dataObj\":"+this.data_response+",\"msg\":\""+this.message+"\"}";
	}	
}