package com.example.task.modules.core.libraries.serviceresponse;


public class ErrorServiceResponse extends ServiceResponse {

    public ErrorServiceResponse(Object dataResponse, String message) {
        this.response = false;
        this.status_code = 200;
        this.message = message;
        this.setDataResponse(dataResponse);
    }

}
