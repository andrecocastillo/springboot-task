package com.example.task.modules.core.libraries.serviceresponse;

public class ErrorServiceResponseException extends Exception{

    private transient ServiceResponseFactory serviceresponse;

    public ErrorServiceResponseException(ServiceResponseFactory serviceresponse) {
        this.serviceresponse = serviceresponse;
    }

    public ServiceResponseFactory getServiceResponse(){
        return this.serviceresponse;
    }
}
