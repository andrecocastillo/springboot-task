package com.example.task.modules.core.libraries.serviceresponse;

import org.springframework.http.ResponseEntity;

public class ServiceResponseFactory{

    ResponseEntity<String> response;
    private String responseType;
    
    public ServiceResponseFactory(String responseType,Object dataResponse,String message){
        if(responseType.equals(ServiceResponseConst.SUCCESS)){
            this.response =  new SuccessServiceResponse(dataResponse,message).responseJSON();
        }else if(responseType.equals(ServiceResponseConst.ERROR)){
            this.response =  new ErrorServiceResponse(dataResponse,message).responseJSON();
        }
        
        this.responseType = responseType;
    }

    public String getResponseType() {
        return this.responseType;
    }

    public ResponseEntity<String> getResponse(){
        return this.response;
    }
}