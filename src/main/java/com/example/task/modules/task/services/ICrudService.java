package com.example.task.modules.task.services;

import com.example.task.modules.core.libraries.serviceresponse.ErrorServiceResponseException;
import com.example.task.modules.core.libraries.serviceresponse.ServiceResponseFactory;
import com.example.task.modules.task.domain.SaveTaskDomain;
import com.example.task.modules.task.domain.UpdateTaskDomain;

public interface ICrudService{
    public ServiceResponseFactory saveTask(SaveTaskDomain saveTaskDom) throws ErrorServiceResponseException;
    public ServiceResponseFactory updateTask(UpdateTaskDomain updateTaskDom) throws ErrorServiceResponseException;
    public ServiceResponseFactory getAllTask() throws ErrorServiceResponseException;
    public ServiceResponseFactory deleteTask(int taskId) throws ErrorServiceResponseException;
}