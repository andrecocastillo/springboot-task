package com.example.task.modules.task.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.example.task.modules.core.libraries.serviceresponse.ErrorServiceResponseException;
import com.example.task.modules.core.libraries.serviceresponse.ServiceResponseConst;
import com.example.task.modules.core.libraries.serviceresponse.ServiceResponseFactory;
import com.example.task.modules.task.constants.StatusEnum;
import com.example.task.modules.task.domain.SaveTaskDomain;
import com.example.task.modules.task.domain.UpdateTaskDomain;
import com.example.task.modules.task.dto.AllTaskDTO;
import com.example.task.modules.task.dto.TaskDTO;
import com.example.task.modules.task.entities.Task;
import com.example.task.modules.task.repositories.TaskRepository;
import com.example.task.modules.task.services.ICrudService;

import org.dozer.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CrudServiceImp implements ICrudService{

    private TaskRepository taskRepository;
	private Mapper mapper;

    public CrudServiceImp(TaskRepository taskRepository, Mapper mapper){
        this.taskRepository = taskRepository;
        this.mapper = mapper;
    }

    @Override
    public ServiceResponseFactory saveTask(SaveTaskDomain saveTaskDom) throws ErrorServiceResponseException {
        // NOTE: Este codigo guarda la tarea sin mapper
        // Task taskValues = new Task();
        // taskValues.setDescription(saveTaskDom.getDescription());
        // taskValues.setDateCreated(new Date());
        // taskValues.setState(StatusEnum.ACTIVE);
        // this.taskRepository.save(taskValues);

        // NOTE: Este codigo guarda la tarea con mapper
        Task taskValues = mapper.map(saveTaskDom, Task.class);
        taskValues.setDateCreated(new Date());
        taskValues.setState(StatusEnum.ACTIVE);
        this.taskRepository.save(taskValues);

        return new ServiceResponseFactory(ServiceResponseConst.SUCCESS, null, "Tarea guardada con exito");
    }

    @Override
    public ServiceResponseFactory updateTask(UpdateTaskDomain updateTaskDom) throws ErrorServiceResponseException {
		Optional<Task> existTask =  this.taskRepository.findById(updateTaskDom.getId());

        if (existTask.isPresent()) {
            Task currentDataTask = existTask.get();
            currentDataTask.setDescription(updateTaskDom.getDescription());
            currentDataTask.setState(updateTaskDom.getState());
            this.taskRepository.save(currentDataTask);

            return new ServiceResponseFactory(ServiceResponseConst.SUCCESS, null, "Tarea actualizada con exito");
        }else{
            throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR, null, "Task no existente"));
        }
    }

    @Override
    public ServiceResponseFactory getAllTask() throws ErrorServiceResponseException {
		List<Task> existTask =  this.taskRepository.findAll();
        
        if (!existTask.isEmpty()) {
            List<TaskDTO> listTask = new AllTaskDTO(existTask).getFinalDTO();
            return new ServiceResponseFactory(ServiceResponseConst.SUCCESS, listTask, "Listado de todas las tareas");
        }else{
            throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR, null, "Task no existente"));
        }
    }

    @Override
    public ServiceResponseFactory deleteTask(int taskId) throws ErrorServiceResponseException {
		Optional<Task> existTask =  this.taskRepository.findById(taskId);

        if (existTask.isPresent()) {
            this.taskRepository.deleteById(taskId);

            return new ServiceResponseFactory(ServiceResponseConst.SUCCESS, null, "Tarea eliminada con exito");
        }else{
            throw new ErrorServiceResponseException(new ServiceResponseFactory(ServiceResponseConst.ERROR, null, "Task no existente"));
        }
    }
}
