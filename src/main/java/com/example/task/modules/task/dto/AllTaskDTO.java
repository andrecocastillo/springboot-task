package com.example.task.modules.task.dto;

import java.util.ArrayList;
import java.util.List;

import com.example.task.modules.task.entities.Task;

public class AllTaskDTO {
  List<Task> listTask;


  public AllTaskDTO(List<Task> listTask){
    this.listTask = listTask;
  }

  public List<TaskDTO> getFinalDTO(){
    List<TaskDTO> finalList = new ArrayList<TaskDTO>();

    for(Task taskInfo : this.listTask){
      TaskDTO currentTask = new TaskDTO();
      currentTask.setId(taskInfo.getId());
      currentTask.setDescription(taskInfo.getDescription());
      currentTask.setState(taskInfo.getState());
      currentTask.setDateCreated(taskInfo.getDateCreated());
      // As example append new field to the response
      currentTask.setFirstLetter(taskInfo.getDescription().charAt(0));
      finalList.add(currentTask);
    }

    return finalList;
  }
  
}
