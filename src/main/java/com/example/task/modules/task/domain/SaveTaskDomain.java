package com.example.task.modules.task.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SaveTaskDomain {

  @NotNull(message="El la descripcion es requerida")
  @Size(min=1, max=1200, message="Ingrese una descripcion valida")
  private String description;
}
