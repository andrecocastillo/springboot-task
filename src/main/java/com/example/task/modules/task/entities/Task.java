package com.example.task.modules.task.entities;

import java.util.Date;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import com.example.task.modules.task.constants.StatusEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "task")
@AttributeOverride(name = "id", column = @Column(name = "task_id"))
@AttributeOverride(name = "description", column = @Column(name = "description"))
@AttributeOverride(name = "state", column = @Column(name = "state"))
@AttributeOverride(name = "dateCreated", column = @Column(name = "date_created"))
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Task {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	private String description;
	@Enumerated(EnumType.STRING)
  private StatusEnum state;
	private Date dateCreated;
}