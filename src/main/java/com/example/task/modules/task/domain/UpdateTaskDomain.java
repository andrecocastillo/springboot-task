package com.example.task.modules.task.domain;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import com.example.task.modules.task.constants.StatusEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UpdateTaskDomain {

  @NotNull(message="El id es requerido")
  private int id;

  @NotNull(message="El la descripcion es requerida")
  @Size(min=1, max=1200, message="Ingrese una descripcion valida")
  private String description;

  @NotNull(message="El estado es requerido")
  private StatusEnum state;
}
