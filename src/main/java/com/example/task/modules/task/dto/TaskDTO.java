package com.example.task.modules.task.dto;

import java.util.Date;

import com.example.task.modules.task.constants.StatusEnum;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class TaskDTO {
	private Integer id;
	private String description;
  private StatusEnum state;
	private Date dateCreated;
	private char firstLetter;
}