package com.example.task.modules.task.controllers;

import com.example.task.modules.core.libraries.serviceresponse.ErrorServiceResponseException;
import com.example.task.modules.core.libraries.serviceresponse.ServiceResponseFactory;
import com.example.task.modules.task.domain.SaveTaskDomain;
import com.example.task.modules.task.domain.UpdateTaskDomain;
import com.example.task.modules.task.services.ICrudService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/api/task/")
public class CrudController {

    @Autowired
    private ICrudService crudService;

    @Operation(summary = "Guardar una tarea")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "Se guardo la tarea con exito."),
        @ApiResponse(responseCode = "400", description = "Error al momento del guardado.")
    })
    @CrossOrigin(origins = "*")
    @PostMapping()
    public ResponseEntity<String> saveTask(@RequestBody SaveTaskDomain saveTaskDom){
        try {
            ServiceResponseFactory serviceResponseFactory = this.crudService.saveTask(saveTaskDom);
            return serviceResponseFactory.getResponse();
        } catch (ErrorServiceResponseException responseException) {
            return responseException.getServiceResponse().getResponse();
        }
    }

    @Operation(summary = "Actualizar una tarea")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "La tarea fue actualizada exitosamente."),
        @ApiResponse(responseCode = "400", description = "Error al momento del actualidar.")
    })
    @CrossOrigin(origins = "*")
    @PutMapping()
    public ResponseEntity<String> updateTask(@RequestBody UpdateTaskDomain updateTaskDom){
        try {
            ServiceResponseFactory serviceResponseFactory = this.crudService.updateTask(updateTaskDom);
            return serviceResponseFactory.getResponse();
        } catch (ErrorServiceResponseException responseException) {
            return responseException.getServiceResponse().getResponse();
        }
    }

    @Operation(summary = "Obtener todas las tareas")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "El litado de las tareas fue exitoso"),
        @ApiResponse(responseCode = "400", description = "Error al momento de consultar")
    })
    @CrossOrigin(origins = "*")
    @GetMapping()
    public ResponseEntity<String> getAllTask(){
        try {
            ServiceResponseFactory serviceResponseFactory = this.crudService.getAllTask();
            return serviceResponseFactory.getResponse();
        } catch (ErrorServiceResponseException responseException) {
            return responseException.getServiceResponse().getResponse();
        }
    }

    @Operation(summary = "Eliminar una tarea")
    @ApiResponses(value = { 
        @ApiResponse(responseCode = "200", description = "La tarea fue eliminada exitosamente"),
        @ApiResponse(responseCode = "400", description = "Error al momento de eliminar")
    })
    @CrossOrigin(origins = "*")
    @DeleteMapping("/{taskId}")
    public ResponseEntity<String> DeleteTask(@PathVariable("taskId") int taskId){
        try {
            ServiceResponseFactory serviceResponseFactory = this.crudService.deleteTask(taskId);
            return serviceResponseFactory.getResponse();
        } catch (ErrorServiceResponseException responseException) {
            return responseException.getServiceResponse().getResponse();
        }
    }
}