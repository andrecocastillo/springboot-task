package com.example.task.modules.task.constants;

public enum StatusEnum {
  ACTIVE, INACTIVE;
}
