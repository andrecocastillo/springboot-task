package com.example.task.modules.task.services.impl;

import com.example.task.modules.core.libraries.serviceresponse.ErrorServiceResponseException;
import com.example.task.modules.core.libraries.serviceresponse.ServiceResponseFactory;
import com.example.task.modules.task.domain.SaveTaskDomain;
import com.example.task.modules.task.entities.Task;
import com.example.task.modules.task.repositories.TaskRepository;
import com.example.task.modules.task.services.ICrudService;

import com.example.task.modules.task.services.impl.CrudServiceImp;
import org.dozer.Mapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = CrudServiceImp.class)
public class CrudServiceImpTest {

  @Autowired
  private ICrudService crudService;

  @MockBean
  private TaskRepository taskRepository;

  @MockBean
  private Mapper mapper;

  @BeforeEach
  void setUp(){
    crudService = new CrudServiceImp(taskRepository, mapper);
  }

  @Test
  void testSaveTask() throws ErrorServiceResponseException {
    SaveTaskDomain saveTaskDom = new SaveTaskDomain();
    saveTaskDom.setDescription("Test descripcion");
    when(mapper.map(any(), any())).thenReturn(new Task());
    ServiceResponseFactory serviceResponseFactory = this.crudService.saveTask(saveTaskDom);
    ResponseEntity<String> response = serviceResponseFactory.getResponse();
    Assertions.assertEquals(200, response.getStatusCodeValue());
    Assertions.assertTrue(response.getBody().contains("Tarea guardada con exito"));
    Assertions.assertTrue(response.getBody().contains("true"));
  }
}